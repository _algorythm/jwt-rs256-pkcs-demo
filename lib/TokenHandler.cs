using System.IdentityModel.Tokens.Jwt;
using System.Security.Cryptography;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;

namespace lib;

public class TokenHandler
{
    private readonly string _validIssuer;

    public TokenHandler(string validIssuer)
    {
        _validIssuer = validIssuer;
    }


    public string Create(string privateKey)
    {
        var now = DateTime.UtcNow;

        using var rsa = RSA.Create();
        rsa.ImportFromPem(privateKey);

        var signingCredentials = new SigningCredentials(new RsaSecurityKey(rsa), SecurityAlgorithms.RsaSha256)
        {
            CryptoProviderFactory = new()
            {
                CacheSignatureProviders = false,
            },
        };

        var claims = new List<Claim>()
        {
            new("key", "value"),
        };

        var jwt = new JwtSecurityToken(
            _validIssuer,
            claims: claims,
            notBefore: now,
            expires: now.AddMinutes(120),
            signingCredentials: signingCredentials);

        return new JwtSecurityTokenHandler().WriteToken(jwt);
    }

    public async Task<bool> Validate(string publicKey, string encodedJwt)
    {
        var rsa = RSA.Create();
        rsa.ImportFromPem(publicKey);

        TokenValidationParameters validationParameters = new()
        {
            ValidIssuer = _validIssuer,
            IssuerSigningKey = new RsaSecurityKey(rsa),
            CryptoProviderFactory = new()
            {
                CacheSignatureProviders = false,
            },
        };

        try
        {
            var handler = new JwtSecurityTokenHandler();
            await handler.ValidateTokenAsync(encodedJwt, validationParameters);
        }
        catch
        {
            return false;
        }

        return true;
    }
}
