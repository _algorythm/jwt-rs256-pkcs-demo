# Token using RS256 and PKCS#1 Token Demo

![Pipelines](https://gitlab.com/_algorythm/jwt-rs256-pkcs-demo/badges/main/pipeline.svg)

How to run:

```bash
./setup.sh
dotnet test
```

