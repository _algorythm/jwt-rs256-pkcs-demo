using System.IO;
using System.Threading.Tasks;
using AutoFixture;
using FluentAssertions;
using lib;
using Xunit;

namespace tests;

public class TokenHandlerTests
{
    [Fact]
    public async Task Test()
    {
        /* Arrange */
        var fixture = new Fixture();
        string privateKey = await File.ReadAllTextAsync("private.pem");
        string publicKey = await File.ReadAllTextAsync("public.pem");
        var tokenHandler = new TokenHandler(fixture.Create<string>());

        string token = tokenHandler.Create(privateKey);

        /* Act */
        bool tokenIsValid = await tokenHandler.Validate(publicKey, token);

        /* Assert */
        tokenIsValid.Should().Be(true);
    }
}
