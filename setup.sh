#!/usr/bin/env bash

openssl genrsa -out tests/private.pem 2048
openssl rsa -in tests/private.pem -pubout -out tests/public.pem

